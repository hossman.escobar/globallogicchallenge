package cl.globallogic.api.domain.controller;

import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.external.externalservice.model.UserExternalDTO;
import cl.globallogic.api.domain.external.externalservice.service.ExternalServiceServices;
import cl.globallogic.api.domain.usescase.users.UserUseCase;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Generated;
import javax.validation.Valid;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-02-12T02:16:38.214242800-03:00[America/Santiago]")
@Controller
@RequestMapping("${openapi.API.base-path:}")
public class SignUpApiExternalController implements SignUpExternalApi {


    @Autowired
    public SignUpApiExternalController(ExternalServiceServices service) {
        this.service = service;
    }

    private final ExternalServiceServices service;

    public ResponseEntity<UserExternalDTO> signExternal(
            @Parameter(name = "UserExternalDTO", description = "", required = true) @Valid @RequestBody UserExternalDTO userExternalDTO
    ) {
        return ResponseEntity.ok(service.signUpExternal(userExternalDTO));
    }
    

}
