package cl.globallogic.api.domain.controller;

import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.usescase.users.LoginUseCase;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;
import javax.annotation.Generated;
import javax.validation.constraints.NotNull;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-02-12T04:30:13.831953100-03:00[America/Santiago]")
@Controller
@RequestMapping("${openapi.API.base-path:}")
public class LoginApiController implements LoginApi {

    private final LoginUseCase loginUseCase;

    @Autowired
    public LoginApiController( LoginUseCase loginUseCase) {
        this.loginUseCase = loginUseCase;
    }


    public ResponseEntity<UserDTO> login(
            @NotNull @Parameter(name = "Authorization", description = "", required = true) @RequestHeader(value = "Authorization", required = true) String authorization
    ){

        return ResponseEntity.ok(loginUseCase.login(authorization));
    }

}
