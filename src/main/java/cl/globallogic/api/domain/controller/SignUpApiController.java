package cl.globallogic.api.domain.controller;

import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.usescase.users.UserUseCase;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.annotation.Generated;
import javax.validation.Valid;
import java.util.Optional;

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-02-12T02:16:38.214242800-03:00[America/Santiago]")
@Controller
@RequestMapping("${openapi.API.base-path:}")
public class SignUpApiController implements SignUpApi {


    @Autowired
    public SignUpApiController(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    private final UserUseCase userUseCase;

    public ResponseEntity<UserDTO> sign(
            @Parameter(name = "UserDTO", description = "", required = true) @Valid @RequestBody UserDTO userDTO
    ) {
        return new ResponseEntity<>(userUseCase.createUser(userDTO), HttpStatus.CREATED);
    }


}
