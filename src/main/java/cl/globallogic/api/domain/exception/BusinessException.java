package cl.globallogic.api.domain.exception;

import org.springframework.http.HttpStatus;

public class BusinessException extends RuntimeException{

    private final HttpStatus httpStatus;
    public BusinessException(String description, Throwable t, HttpStatus httpStatus) {
        super(description, t);
        this.httpStatus = httpStatus;
    }

    public BusinessException(String description, HttpStatus httpStatus) {
        super(description);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
