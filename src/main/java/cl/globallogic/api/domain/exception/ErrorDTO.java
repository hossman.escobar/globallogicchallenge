package cl.globallogic.api.domain.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDTO {
    @JsonProperty("mensaje")
    private String mensaje;
}
