package cl.globallogic.api.domain.constants;

public enum BusinessErrorMessages {
    USER_NOT_FOUND(0, "User not found"),
    INCORRECT_PASSWORD_FORMAT(2, "Incorrect password format"),

    INCORRECT_EMAIL_FORMAT(3, "Incorrect email format"),


    ERROR_CREATING_USER(4, "Error creating user");


    private final int code;
    private final String description;

    private BusinessErrorMessages(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

}