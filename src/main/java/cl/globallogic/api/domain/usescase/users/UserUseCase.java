package cl.globallogic.api.domain.usescase.users;

import cl.globallogic.api.domain.exception.BusinessException;
import cl.globallogic.api.domain.constants.BusinessErrorMessages;
import cl.globallogic.api.infrastructure.exception.RepositoryException;
import cl.globallogic.api.infrastructure.gateway.UserGateway;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.app.utils.CommonUtils;
import com.password4j.Password;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserUseCase {
    private final UserGateway userGateway;

    @Value("${userusecase.password.format}")
    private String passwordFormat;

    @Value("${userusecase.email.format}")
    private String emailFormat;

    public UserUseCase(UserGateway userGateway) {
        this.userGateway = userGateway;
    }

    public UserDTO createUser(UserDTO userDTO) throws BusinessException{

        if (!CommonUtils.validExpresion(userDTO.getEmail(), emailFormat))
            throw new BusinessException(BusinessErrorMessages.INCORRECT_EMAIL_FORMAT.getDescription(), HttpStatus.BAD_REQUEST);

        if (!CommonUtils.validExpresion(userDTO.getPassword(), passwordFormat))
            throw new BusinessException(BusinessErrorMessages.INCORRECT_PASSWORD_FORMAT.getDescription(), HttpStatus.BAD_REQUEST);



        userDTO.setPassword(Password.hash(userDTO.getPassword()).withPBKDF2().getResult());
        UserDTO response = null;

        try {
             response = userGateway.createdUser(userDTO);
        } catch ( RepositoryException ex){
            throw new BusinessException(BusinessErrorMessages.ERROR_CREATING_USER.getDescription()+","+ex.getMessage(), ex, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

}
