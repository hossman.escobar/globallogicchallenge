package cl.globallogic.api.domain.usescase.users;

import cl.globallogic.api.domain.constants.BusinessErrorMessages;
import cl.globallogic.api.domain.exception.BusinessException;
import cl.globallogic.api.infrastructure.exception.RepositoryException;
import cl.globallogic.api.infrastructure.gateway.UserGateway;
import cl.globallogic.api.domain.dto.UserDTO;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class LoginUseCase {

    private final UserGateway userGateway;

    public LoginUseCase(UserGateway userGateway) {
        this.userGateway = userGateway;
    }

    public UserDTO login(String token) throws BusinessException{

        try {
            return userGateway.authenticate(token);
        } catch (RepositoryException ex){
            throw new BusinessException(BusinessErrorMessages.USER_NOT_FOUND.getDescription(), ex , HttpStatus.NO_CONTENT);
        }

    }


}
