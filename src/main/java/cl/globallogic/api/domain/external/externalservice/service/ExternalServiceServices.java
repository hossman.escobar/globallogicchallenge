package cl.globallogic.api.domain.external.externalservice.service;

import cl.globallogic.api.app.external.connection.GenericRestClient;
import cl.globallogic.api.domain.external.externalservice.exception.ExternalServiceException;
import cl.globallogic.api.domain.external.externalservice.model.UserExternalDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.HashMap;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "service.externalservice")
@Service
@Slf4j
public class ExternalServiceServices {

    private static HashMap<String, String> headers;

    public static HashMap<String, String> getHeaders() {
        return ExternalServiceServices.headers;
    }

    public void setHeaders(HashMap<String, String> headers) {
        ExternalServiceServices.headers = headers;
    }

    private static String signupUrl;

    @Value("${service.externalservice.endpoint.signup.url}")
    public void setExternalServiceSignUpUrl(String signupUrl) {
        ExternalServiceServices.signupUrl = signupUrl;
    }

    private static String externalServiceBaseUrl;

    @Value("${service.externalservice.base.url}")
    public void setExternalServiceBaseUrlUrl(String externalServiceBaseUrl) {
        ExternalServiceServices.externalServiceBaseUrl = externalServiceBaseUrl;
    }

    private ExternalServiceServices() {

    }

    public static UserExternalDTO signUpExternal(UserExternalDTO userExternalDTO) throws ExternalServiceException{

        UserExternalDTO responseBody = new UserExternalDTO();
        try {
            responseBody = new GenericRestClient<UserExternalDTO, UserExternalDTO>()
                    .execute(
                            externalServiceBaseUrl + signupUrl,
                            HttpMethod.POST,
                            userExternalDTO,
                            UserExternalDTO.class, GenericRestClient.getHttpHeaders(getHeaders()))
                    .getBody();
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            throw new ExternalServiceException(e.getStatusCode(),
                    e.getMessage(), e.getCause());
        } catch (Exception e) {
            throw new ExternalServiceException(HttpStatus.INTERNAL_SERVER_ERROR,
                    e.getMessage(), e.getCause());
        }
        return responseBody;
    }


}
