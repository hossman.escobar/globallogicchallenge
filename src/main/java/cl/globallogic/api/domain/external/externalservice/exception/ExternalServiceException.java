package cl.globallogic.api.domain.external.externalservice.exception;

import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

public class ExternalServiceException extends RuntimeException {

    private static final long serialVersionUID = 4577853273176571331L;

    private final BigDecimal status;
    private final HttpStatus httpStatus;

    public ExternalServiceException(BigDecimal status) {
        this.status = status;
        this.httpStatus = null;
    }

    public ExternalServiceException(BigDecimal status, HttpStatus httpStatus) {
        this.status = status;
        this.httpStatus = httpStatus;
    }

    public ExternalServiceException(BigDecimal status, String mensaje, HttpStatus httpStatus) {
        super(mensaje);
        this.status = status;
        this.httpStatus = httpStatus;

    }

    public ExternalServiceException(HttpStatus httpStatus, String mensaje) {
        super(mensaje);
        this.httpStatus = httpStatus;
        this.status = null;
    }

    public ExternalServiceException(HttpStatus httpStatus, String mensaje, Throwable t) {
        super(mensaje, t);
        this.httpStatus = httpStatus;
        this.status = null;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public BigDecimal getStatus() {
        return status;
    }
}
