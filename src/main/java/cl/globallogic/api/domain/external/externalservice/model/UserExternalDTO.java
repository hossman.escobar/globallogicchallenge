package cl.globallogic.api.domain.external.externalservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.format.annotation.DateTimeFormat;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-02-12T04:30:13.831953100-03:00[America/Santiago]")
public class UserExternalDTO {

  @JsonProperty("id")
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private UUID id;

  @JsonProperty("created")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private OffsetDateTime created;

  @JsonProperty("lastLogin")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private OffsetDateTime lastLogin;

  @JsonProperty("token")
  private String token;

  @JsonProperty("name")
  private String name;

  @JsonProperty("email")
  private String email;

  @JsonProperty("password")
  private String password;

  @JsonProperty("isActive")
  private Boolean isActive = true;

  @JsonProperty("phones")
  @Valid
  private List<PhoneExternalDTO> phones = null;

  public UserExternalDTO id(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   */
  @Valid
  @Schema(name = "id", example = "f737c061-c997-4c57-ae83-b85ec14777e2", required = false)
  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UserExternalDTO created(OffsetDateTime created) {
    this.created = created;
    return this;
  }

  /**
   * Get created
   * @return created
   */
  @Valid
  @Schema(name = "created", required = false)
  public OffsetDateTime getCreated() {
    return created;
  }

  public void setCreated(OffsetDateTime created) {
    this.created = created;
  }

  public UserExternalDTO lastLogin(OffsetDateTime lastLogin) {
    this.lastLogin = lastLogin;
    return this;
  }

  /**
   * Get lastLogin
   * @return lastLogin
   */
  @Valid
  @Schema(name = "lastLogin", required = false)
  public OffsetDateTime getLastLogin() {
    return lastLogin;
  }

  public void setLastLogin(OffsetDateTime lastLogin) {
    this.lastLogin = lastLogin;
  }

  public UserExternalDTO token(String token) {
    this.token = token;
    return this;
  }

  /**
   * Get token
   * @return token
   */

  @Schema(name = "token", example = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJob3NzbWFuMzQifQ.7k-MyxPTqgmbTrH2ozycPCGyDsaff3HapXN2kMfkh3s", required = false)
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public UserExternalDTO name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Get name
   * @return name
   */

  @Schema(name = "name", required = false)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public UserExternalDTO email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
   */
  @Email
  @Schema(name = "email", example = "hossman.escobar@globaltest.cl", required = false)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public UserExternalDTO password(String password) {
    this.password = password;
    return this;
  }

  /**
   * Get password
   * @return password
   */

  @Schema(name = "password", example = "globalpassword", required = false)
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public UserExternalDTO isActive(Boolean isAactive) {
    this.isActive = isAactive;
    return this;
  }

  /**
   * Get isactive
   * @return isactive
   */

  @Schema(name = "isActive", required = false)
  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isAactive) {
    this.isActive = isAactive;
  }

  public UserExternalDTO phones(List<PhoneExternalDTO> phones) {
    this.phones = phones;
    return this;
  }

  public UserExternalDTO addPhonesItem(PhoneExternalDTO phonesItem) {
    if (this.phones == null) {
      this.phones = new ArrayList<>();
    }
    this.phones.add(phonesItem);
    return this;
  }

  /**
   * Get phones
   * @return phones
   */
  @Valid
  @Schema(name = "phones", required = false)
  public List<PhoneExternalDTO> getPhones() {
    return phones;
  }

  public void setPhones(List<PhoneExternalDTO> phones) {
    this.phones = phones;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserExternalDTO userExternalDTO = (UserExternalDTO) o;
    return Objects.equals(this.id, userExternalDTO.id) &&
            Objects.equals(this.created, userExternalDTO.created) &&
            Objects.equals(this.lastLogin, userExternalDTO.lastLogin) &&
            Objects.equals(this.token, userExternalDTO.token) &&
            Objects.equals(this.name, userExternalDTO.name) &&
            Objects.equals(this.email, userExternalDTO.email) &&
            Objects.equals(this.password, userExternalDTO.password) &&
            Objects.equals(this.isActive, userExternalDTO.isActive) &&
            Objects.equals(this.phones, userExternalDTO.phones);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, created, lastLogin, token, name, email, password, isActive, phones);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserDTO {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    lastLogin: ").append(toIndentedString(lastLogin)).append("\n");
    sb.append("    token: ").append(toIndentedString(token)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    password: ").append(toIndentedString(password)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("    phones: ").append(toIndentedString(phones)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}



