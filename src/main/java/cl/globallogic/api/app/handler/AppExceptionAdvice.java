package cl.globallogic.api.app.handler;

import cl.globallogic.api.domain.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class AppExceptionAdvice {

    @ExceptionHandler(value = BusinessException.class)
    public ResponseEntity<ErrorDTO> handlerBusiness(BusinessException ex){
        ErrorDTO error = new ErrorDTO();
        error.setMensaje(ex.getMessage());
        log.error("Error:", ex);
        return new ResponseEntity<>(error, ex.getHttpStatus());
    }
}
