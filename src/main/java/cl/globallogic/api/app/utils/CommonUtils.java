package cl.globallogic.api.app.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {

    private CommonUtils (){}

    public static boolean validExpresion(String expression, String regex){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(expression);
        return matcher.matches();

    }

}
