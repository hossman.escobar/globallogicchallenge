package cl.globallogic.api.app.config;

import cl.globallogic.api.infrastructure.gateway.AuthenticationGateway;
import cl.globallogic.api.infrastructure.gateway.UserGateway;
import cl.globallogic.api.infrastructure.adapter.JwtAuthenticationAdapter;
import cl.globallogic.api.infrastructure.adapter.UserServiceAdapter;
import cl.globallogic.api.infrastructure.repository.UserRepository;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCaseConfig {

    @Bean
    OpenAPI apiInfo() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Test")
                                .description("Test Global Logic")
                                .contact(
                                        new Contact()
                                                .name("Hossman Escobar")
                                                .email("hossman.escobar@globaltest.cl")
                                )
                                .version("1.0.0")
                )
                ;
    }

}
