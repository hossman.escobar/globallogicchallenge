package cl.globallogic.api.app.external.connection;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Slf4j
@Component
public class GenericRestClient<T, V> {


    private final RestTemplate restTemplate = new RestTemplate();

    public ResponseEntity<V> execute(String url,
                                     HttpMethod httpMethod,
                                     MultiValueMap<String, String> dataEncode,
                                     Class<V> genericResponseClass, HttpHeaders headersHttp) {


        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(dataEncode, headersHttp);
        ResponseEntity<V> response = restTemplate.exchange(url, httpMethod,
                entity, genericResponseClass);

        printPrettyJson(response, "RESPONSE");
        return response;

    }

    public ResponseEntity<V> execute(String url,
                                     HttpMethod httpMethod, T request,
                                     Class<V> genericResponseClass, HttpHeaders headersHttp) {

        log.info("[url]:{} [headers]: {}", url, headersHttp);


        HttpEntity<T> entity = new HttpEntity<>(request, headersHttp);

        printPrettyJson(request, "ExternalRequest:"+request.getClass().getSimpleName());
        ResponseEntity<V> response = restTemplate.exchange(url, httpMethod,
                entity, genericResponseClass);
        printPrettyJson(response, "ExternalResponse:"+genericResponseClass.getSimpleName());
        return response;

    }

    private void printPrettyJson(Object data, String nameObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
            mapper.registerModule(new JavaTimeModule());
            String requestJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
            log.info("{}: \n{}", nameObject, requestJson);
        } catch (JsonProcessingException e) {
            log.error("JsonProcessingException", e);
            e.printStackTrace();
        }
    }

    public static HttpHeaders getHttpHeaders(HashMap<String, String> headers) {
        HttpHeaders headersHttp = new HttpHeaders();
        headers.forEach(
                headersHttp::add);
        return headersHttp;
    }
}