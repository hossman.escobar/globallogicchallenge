package cl.globallogic.api.app.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.*;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.UUID;

@Slf4j
@Component
@Order(2)
@SuppressWarnings("ALL")
public class LoggingFilter implements Filter {

    @Value("${show.headers}")
    private boolean showHeaders = true;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        // Set trace id to logger
        MDC.put("TID", UUID.randomUUID().toString().substring(0, 8));
        // Set process id to logger
        MDC.put("PID", ManagementFactory.getRuntimeMXBean().getName());

        filterChain.doFilter(request, response);

        if (request instanceof ContentCachingRequestWrapper) {
            ContentCachingRequestWrapper requestWrapper = (ContentCachingRequestWrapper) request;
            String payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(
                    new String(requestWrapper.getContentAsByteArray(), StandardCharsets.UTF_8)));

            log.info("InternalRequest [{}: {}] \n{}", requestWrapper.getMethod(), requestWrapper.getRequestURI(), payload);

            if(showHeaders){
                Enumeration<String> headerNames = requestWrapper.getHeaderNames();
                while (headerNames.hasMoreElements()) {
                    String headerName = headerNames.nextElement();
                    log.info("Header [{} : {}]", headerName, requestWrapper.getHeader(headerName));
                }
            }

        }
        if (response instanceof ContentCachingResponseWrapper) {
            ContentCachingResponseWrapper responseWrapper = (ContentCachingResponseWrapper) response;
            String payload = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapper.readTree(
                    new String(responseWrapper.getContentAsByteArray(), StandardCharsets.UTF_8)));
            log.info("InternalResponse [ {} ] \n{}", responseWrapper.getStatus(), payload);

        }

        MDC.clear();

    }

}