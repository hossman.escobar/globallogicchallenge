package cl.globallogic.api.infrastructure.entity;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@ToString
@Table(name = "user",uniqueConstraints = {@UniqueConstraint(columnNames={"email"})})
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private UUID id;
    @Column
    private String name;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String phones;
    @Column
    private OffsetDateTime created;
    @Column
    private OffsetDateTime lastLogin;
    @Column
    private boolean isActive;
    @Column
    private String token;

}
