package cl.globallogic.api.infrastructure.constants;

public enum RepositoryErrorMessages {

    USER_NOT_FOUND_ON_DB(0, "User not found on db"),
    USER_ALREADY_REGISTERED(1, "User already exist on db");

    private final int code;
    private final String description;

    private RepositoryErrorMessages(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

}