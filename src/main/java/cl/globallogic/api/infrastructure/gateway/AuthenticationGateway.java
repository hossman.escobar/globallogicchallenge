package cl.globallogic.api.infrastructure.gateway;

public interface AuthenticationGateway {

    String authorize(String email);
}
