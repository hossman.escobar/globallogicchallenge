package cl.globallogic.api.infrastructure.gateway;

import cl.globallogic.api.domain.dto.UserDTO;

public interface UserGateway {


    UserDTO createdUser (UserDTO userDTO);

    UserDTO authenticate (String token);



}
