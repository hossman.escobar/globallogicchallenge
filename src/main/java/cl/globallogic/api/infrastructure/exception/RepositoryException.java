package cl.globallogic.api.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class RepositoryException extends RuntimeException{


    public RepositoryException(String description, Throwable t) {
        super(description, t);
    }

    public RepositoryException(String description) {
        super(description);

    }


}