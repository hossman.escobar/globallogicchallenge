package cl.globallogic.api.infrastructure.repository;

import cl.globallogic.api.infrastructure.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, UUID> {

    UserEntity findByEmail(String email);
    UserEntity findByToken(String token);
}
