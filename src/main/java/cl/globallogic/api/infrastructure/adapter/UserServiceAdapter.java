package cl.globallogic.api.infrastructure.adapter;

import cl.globallogic.api.infrastructure.exception.RepositoryException;
import cl.globallogic.api.infrastructure.constants.RepositoryErrorMessages;
import cl.globallogic.api.infrastructure.gateway.AuthenticationGateway;
import cl.globallogic.api.infrastructure.gateway.UserGateway;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.infrastructure.repository.UserRepository;
import cl.globallogic.api.infrastructure.entity.UserEntity;
import cl.globallogic.api.infrastructure.mapper.UserAdapterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.Optional;


@Component
public class UserServiceAdapter implements UserGateway {

    private final UserRepository userRepository;
    private final AuthenticationGateway authenticationGateway;

    private final UserAdapterMapper userAdapterMapper;

    public UserServiceAdapter(UserRepository userRepository,
                              AuthenticationGateway authenticationGateway, UserAdapterMapper userAdapterMapper) {
        this.userRepository = userRepository;
        this.authenticationGateway = authenticationGateway;
        this.userAdapterMapper = userAdapterMapper;
    }

    public UserDTO createdUser(UserDTO userDTO) throws RepositoryException {
        UserEntity userEntity = userRepository.findByEmail(userDTO.getEmail());
        if (Objects.nonNull(userEntity))
            throw new RepositoryException(RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription());

        Optional.ofNullable(userDTO.getEmail())
                .map(authenticationGateway::authorize)
                .ifPresent(userDTO::setToken);

        userEntity = userAdapterMapper.convert(userDTO);
        userEntity = userRepository.save(userEntity);
        return userAdapterMapper.convert(userEntity);
    }

    @Override
    public UserDTO authenticate(String token) throws RepositoryException{
        UserEntity entity = userRepository.findByToken(token);

        if(Objects.isNull(entity)) {
            throw new RepositoryException(RepositoryErrorMessages.USER_NOT_FOUND_ON_DB.getDescription());
        }

        OffsetDateTime offsetdatetime
                = OffsetDateTime.now();

        Optional.ofNullable(entity.getEmail())
                .map(authenticationGateway::authorize)
                        .ifPresent(entity::setToken);
        entity.setLastLogin(offsetdatetime);

        return userAdapterMapper.convert(userRepository.save(entity));
    }


}
