package cl.globallogic.api.infrastructure.adapter;

import cl.globallogic.api.infrastructure.gateway.AuthenticationGateway;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import java.security.Key;

@Component
public class JwtAuthenticationAdapter implements AuthenticationGateway {

    @Override
    public String authorize(String email) {
        Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
        return Jwts.builder().setSubject(email).signWith(key).compact();
    }
}