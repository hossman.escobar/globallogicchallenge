package cl.globallogic.api.infrastructure.mapper;

import cl.globallogic.api.domain.dto.PhoneDTO;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.infrastructure.entity.UserEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.List;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface UserAdapterMapper {

    UserAdapterMapper INSTANCE = getMapper(UserAdapterMapper.class);

    @Mapping(target = "phones", source = "userDTO.phones", qualifiedByName = "mapPhonesDTO")
    UserEntity convert(UserDTO userDTO);

    @Mapping(target = "phones", source = "userEntity.phones", qualifiedByName = "mapPhonesEntity")
    UserDTO convert(UserEntity userEntity);

    @Named("mapPhonesDTO")
    default String mapPhonesDTO(List<PhoneDTO>  phonesList) throws JsonProcessingException {

            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(phonesList);

    }

    @Named("mapPhonesEntity")
    default ArrayList<PhoneDTO> mapPhonesEntity(String phones) throws JsonProcessingException {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(phones, ArrayList.class);

    }
}
