package cl.globallogic.api.infraestructure.adapter;

import cl.globallogic.api.domain.constants.BusinessErrorMessages;
import cl.globallogic.api.domain.dto.PhoneDTO;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.exception.BusinessException;
import cl.globallogic.api.domain.usescase.users.LoginUseCase;
import cl.globallogic.api.domain.usescase.users.UserUseCase;
import cl.globallogic.api.infrastructure.adapter.UserServiceAdapter;
import cl.globallogic.api.infrastructure.constants.RepositoryErrorMessages;
import cl.globallogic.api.infrastructure.entity.UserEntity;
import cl.globallogic.api.infrastructure.exception.RepositoryException;
import cl.globallogic.api.infrastructure.gateway.AuthenticationGateway;
import cl.globallogic.api.infrastructure.gateway.UserGateway;
import cl.globallogic.api.infrastructure.mapper.UserAdapterMapper;

import cl.globallogic.api.infrastructure.repository.UserRepository;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Objects;

import static cl.globallogic.api.infrastructure.mapper.UserAdapterMapper.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;


@SpringBootTest
class UserServiceAdapterTest {

    private UserServiceAdapter adapter;

    @MockBean
    private UserRepository repository;

    @MockBean
    private UserAdapterMapper mapper;

    @Autowired
    public UserServiceAdapterTest(UserServiceAdapter adapter) {
        this.adapter = adapter;
    }


    private String token;
    private UserDTO userDTO ;

    private UserEntity userEntity;
    @BeforeEach
    void setUp() {
        // Aca tampoco importan los valores porqe ya paso por la capa de validaicon
        EasyRandom easyRandom = new EasyRandom();
        this.token = easyRandom.nextObject(String.class);
        this.userDTO = easyRandom.nextObject(UserDTO.class);
        this.userEntity = easyRandom.nextObject(UserEntity.class);
    }

    @Test
    void authorizeSuccessfulTest() {


        // No se porque no funciona al mockear auth
        //when(authgateway.authorize(token)).thenReturn(any(String.class));
        when(repository.findByToken(token)).thenReturn(userEntity);
        when(repository.save(userEntity)).thenReturn(userEntity);
        when(mapper.convert(userEntity)).thenReturn(userDTO);
        assertEquals(userDTO, adapter.authenticate(token));
    }

    @Test
    void authorizeNotFoundTest() {

        when(repository.findByToken(token)).thenReturn(null);

        RepositoryException thrown = assertThrows(
                RepositoryException.class,
                () -> adapter.authenticate(token),
                RepositoryErrorMessages.USER_NOT_FOUND_ON_DB.getDescription()
        );
        assertTrue(thrown.getMessage().contains(RepositoryErrorMessages.USER_NOT_FOUND_ON_DB.getDescription()));
    }


    @Test
    void createdUserExistOnDbTest() {

        //Forma incorrecta, porque fuerzas la exception antes que ocurra
        //when(repository.findByEmail(userDTO.getEmail())).thenThrow(new RepositoryException(RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription()));

        //Forma correcta, se da la exception de forma natural al mockear el entity con valores
        when(repository.findByEmail(userDTO.getEmail())).thenReturn(userEntity);

        RepositoryException thrown = assertThrows(
                RepositoryException.class,
                () -> adapter.createdUser(userDTO),
                RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription()
        );
        assertTrue(thrown.getMessage().contains(RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription()));
    }

    @Test
    void createdUserSuccessfulTest() {

        // No se porque no funciona al mockear auth
        //when(authgateway.authorize(userDTO.getEmail())).thenReturn(any(String.class));
        when(repository.findByEmail(userDTO.getEmail())).thenReturn(null);
        // Tiene que retornar null para que entienda que no esta regitrado el objeto
        when(mapper.convert(userDTO)).thenReturn(userEntity);
        when(repository.save(userEntity)).thenReturn(userEntity);
        when(mapper.convert(userEntity)).thenReturn(userDTO);
        assertEquals(userDTO, adapter.createdUser(userDTO));
    }

}