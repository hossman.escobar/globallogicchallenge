package cl.globallogic.api.domain.usescase.users;

import cl.globallogic.api.domain.constants.BusinessErrorMessages;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.exception.BusinessException;
import cl.globallogic.api.infrastructure.constants.RepositoryErrorMessages;
import cl.globallogic.api.infrastructure.exception.RepositoryException;
import cl.globallogic.api.infrastructure.gateway.UserGateway;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@SpringBootTest
class UserUseCaseTest {


    public static final String EMAIL_OK = "hossman.escobar@usach.cl";
    public static final String EMAIL_NOK = "hossman.escobar";

    public static final String PASSWORD_NOK = "";
    public static final String PASSWORD_OK = "12";

    private final UserUseCase userUseCase;

    @MockBean
    private UserGateway gateway;

    @Autowired
    public UserUseCaseTest(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }


    @Test
    void saveUserSuccessfulTest() {
        // Aqui si importan los valores de los datos porque lo que estoy probando userUseCase, si tiene las  validaciones

        UserDTO user = new UserDTO()
                .email(EMAIL_OK)
                .password(PASSWORD_OK);

        when(gateway.createdUser(user)).thenReturn(user);
        assertEquals(user, userUseCase.createUser(user));
    }

    @Test
    void saveUserIncorrectEmailFormatTest() {
        UserDTO user = new UserDTO()
                .email(EMAIL_NOK)
                .password(PASSWORD_OK);

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> userUseCase.createUser(user),
                BusinessErrorMessages.INCORRECT_EMAIL_FORMAT.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.INCORRECT_EMAIL_FORMAT.getDescription()));
    }

    @Test
    void saveUserIncorrectPasswordFormatTest() {
        UserDTO user = new UserDTO()
                .email(EMAIL_OK)
                .password(PASSWORD_NOK);

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> userUseCase.createUser(user),
                BusinessErrorMessages.INCORRECT_PASSWORD_FORMAT.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.INCORRECT_PASSWORD_FORMAT.getDescription()));
    }

    @Test
    void saveUserAlreadyExistTest() {
        UserDTO user = new UserDTO()
                .email(EMAIL_OK)
                .password(PASSWORD_OK);

        when(gateway.createdUser(user)).thenThrow(new RepositoryException(RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription()));

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> userUseCase.createUser(user),
                BusinessErrorMessages.ERROR_CREATING_USER.getDescription()+","+RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.ERROR_CREATING_USER.getDescription()+","+RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription()));
    }
}