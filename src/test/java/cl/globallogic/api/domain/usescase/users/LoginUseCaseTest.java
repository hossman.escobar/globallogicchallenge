package cl.globallogic.api.domain.usescase.users;

import cl.globallogic.api.domain.constants.BusinessErrorMessages;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.exception.BusinessException;
import cl.globallogic.api.infrastructure.constants.RepositoryErrorMessages;
import cl.globallogic.api.infrastructure.exception.RepositoryException;
import cl.globallogic.api.infrastructure.gateway.UserGateway;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@SpringBootTest
class LoginUseCaseTest {


    public static final String EMAIL_OK = "hossman.escobar@usach.cl";
    public static final String PASSWORD_OK = "12";
    public static final String USER_OK = "user";
    private final LoginUseCase service;

    @MockBean
    private UserGateway gateway;

    @Autowired
    public LoginUseCaseTest(LoginUseCase service) {
        this.service = service;
    }


    private String token;
    private UserDTO userDTO ;
    @BeforeEach
    void setUp() {
        EasyRandom easyRandom = new EasyRandom();
        this.token = easyRandom.nextObject(String.class);
        this.userDTO = easyRandom.nextObject(UserDTO.class);
    }

    @Test
    void loginSuccessfulTest() {
        when(gateway.authenticate(token)).thenReturn(userDTO);
        assertEquals(userDTO, service.login(token));
    }


   @Test
    void loginNotFoundTest() {

       when(gateway.authenticate(token)).thenThrow(new RepositoryException(RepositoryErrorMessages.USER_NOT_FOUND_ON_DB.getDescription()));

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> service.login(token),
                BusinessErrorMessages.USER_NOT_FOUND.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.USER_NOT_FOUND.getDescription()));
    }
    

}