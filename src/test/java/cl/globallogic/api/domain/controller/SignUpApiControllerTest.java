package cl.globallogic.api.domain.controller;

import cl.globallogic.api.domain.constants.BusinessErrorMessages;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.exception.BusinessException;
import cl.globallogic.api.domain.usescase.users.LoginUseCase;
import cl.globallogic.api.domain.usescase.users.UserUseCase;
import cl.globallogic.api.infrastructure.constants.RepositoryErrorMessages;
import cl.globallogic.api.infrastructure.exception.RepositoryException;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@SpringBootTest
class SignUpApiControllerTest {


    private final SignUpApiController controller;

    @MockBean
    private UserUseCase service;

    @Autowired
    public SignUpApiControllerTest(SignUpApiController controller) {
        this.controller = controller;
    }

    private String token;
    private UserDTO userDTO ;
    @BeforeEach
    void setUp() {
        EasyRandom easyRandom = new EasyRandom();
        this.token = easyRandom.nextObject(String.class);
        this.userDTO = easyRandom.nextObject(UserDTO.class);
    }

    @Test
    void saveUserControllerSuccessfulTest() {

        //Puedo bypesar los email y pass validos porque estoy probando controller, no logica de negocio

        when(service.createUser(userDTO))
                .thenReturn(userDTO);

        assertEquals(userDTO, service.createUser(userDTO));
        assertEquals(HttpStatus.CREATED,
                controller.sign(userDTO).getStatusCode());

    }

    @Test
    void saveUserControllerIncorrectEmailFormatTest() {

        when(service.createUser(userDTO))
                .thenThrow(new BusinessException(
                        BusinessErrorMessages.INCORRECT_EMAIL_FORMAT.getDescription(),
                        HttpStatus.BAD_REQUEST));

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> controller.sign(userDTO),
                BusinessErrorMessages.INCORRECT_EMAIL_FORMAT.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.INCORRECT_EMAIL_FORMAT.getDescription()));
        assertEquals(HttpStatus.BAD_REQUEST, thrown.getHttpStatus());

    }

    @Test
    void saveUserControllerIncorrectPasswordFormatTest() {

        when(service.createUser(userDTO))
                .thenThrow(new BusinessException(
                        BusinessErrorMessages.INCORRECT_PASSWORD_FORMAT.getDescription(),
                        HttpStatus.BAD_REQUEST));

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> controller.sign(userDTO),
                BusinessErrorMessages.INCORRECT_PASSWORD_FORMAT.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.INCORRECT_PASSWORD_FORMAT.getDescription()));
        assertEquals(HttpStatus.BAD_REQUEST, thrown.getHttpStatus());
    }

    @Test void saveUserControllerAlreadyExistTest() {

        when(service.createUser(userDTO))
                .thenThrow(new BusinessException(
                        BusinessErrorMessages.ERROR_CREATING_USER.getDescription(),
                        new RepositoryException(RepositoryErrorMessages.USER_ALREADY_REGISTERED.getDescription()),
                        HttpStatus.INTERNAL_SERVER_ERROR));

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> controller.sign(userDTO),
                BusinessErrorMessages.ERROR_CREATING_USER.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.ERROR_CREATING_USER.getDescription()));
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, thrown.getHttpStatus());
    }

}