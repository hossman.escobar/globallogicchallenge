package cl.globallogic.api.domain.controller;

import cl.globallogic.api.domain.constants.BusinessErrorMessages;
import cl.globallogic.api.domain.dto.UserDTO;
import cl.globallogic.api.domain.exception.BusinessException;
import cl.globallogic.api.domain.usescase.users.LoginUseCase;
import cl.globallogic.api.infrastructure.constants.RepositoryErrorMessages;
import cl.globallogic.api.infrastructure.exception.RepositoryException;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@SpringBootTest
class LoginApiControllerTest {

    private final LoginApiController controller;

    @MockBean
    private LoginUseCase service;

    @Autowired
    public LoginApiControllerTest(LoginApiController controller) {
        this.controller = controller;
    }

    private String token;
    private UserDTO userDTO ;
    @BeforeEach
    void setUp() {
        EasyRandom easyRandom = new EasyRandom();
        this.token = easyRandom.nextObject(String.class);
        this.userDTO = easyRandom.nextObject(UserDTO.class);
    }

    @Test
    void loginSuccessfulTest() {

        when(service.login(token))
                .thenReturn(userDTO);
        assertEquals(userDTO, service.login(token));
        assertEquals(HttpStatus.OK,
                controller.login(any(String.class)).getStatusCode());

    }

    @Test
    void loginNotFoundTest() {

        when(service.login(token))
                .thenThrow(new BusinessException(
                        BusinessErrorMessages.USER_NOT_FOUND.getDescription(),
                        new RepositoryException(RepositoryErrorMessages.USER_NOT_FOUND_ON_DB.getDescription()),
                        HttpStatus.NO_CONTENT));

        BusinessException thrown = assertThrows(
                BusinessException.class,
                () -> controller.login(token),
                BusinessErrorMessages.USER_NOT_FOUND.getDescription()
        );

        assertTrue(thrown.getMessage().contains(BusinessErrorMessages.USER_NOT_FOUND.getDescription()));
        assertEquals(HttpStatus.NO_CONTENT, thrown.getHttpStatus());

    }

}