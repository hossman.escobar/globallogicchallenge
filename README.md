# GlobalLogicChallenge

## NOTA IMPORANTE
Todo la capa "external" no es parte de este desafio y ha sido construido a modo educativo para mis alumnos,
este repo ira creciendo con buenas practicas y patrones.

## Pre-requisitos
El proyecto fue creado para Java8 por lo que debes tenerlo instalado, si tienes una version superior
el archivo build.gradle tiene un toolchain para poder realizar la adaptacion a la version correcta

No se necesitan script de base de datos ya que se utiliza una base de datos volatil.

## Configs necesarias
Expresiones regulares son parametrizables editando el archivo application.properties

Valores por defecto:
```bash
userusecase.password.format = .{1,}
userusecase.email.format = ^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$
```

## Compilacion

Para compilar situarse en la raiz del proyecto y ejecutar el siguiente comando
(tambien puedes hacerlo con tu IDE preferido, recuerda setear la version a Java8)

```bash
./gradlew build
```

## Ejecucion
Para ejecutar situarse en la raiz del proyecto y ejecutar el siguiente comando

```bash
java -jar .\build\libs\globallogicchallenge-0.0.1-SNAPSHOT.jar
```

## Pruebas desde postman
Para pruebas puedes importar el proyecto postman ubicado en
```bash
.\postman\GlobalLogic.postman_collection.json
```
NOTA: Si utilizas el endpoint de registro de usuario, automaticamente guardara el token para el request del login. Por lo que
te recomiendo probar esos endpoints juntos.

En el proyecto se encuentran algunos "test funcionales" que validan el statusHttp, sin embargo,
necesitan agregarse más test.

## Pruebas desde newman
Situarse en carpeta de la collection postman e instalar newman y htmlextra reporter
(Para esto se debe tener instalada las dependencias de npm y node)

```bash
npm install -g newman
npm install -g newman-reporter-htmlextra
newman run .\GlobalLogic.postman_collection.json -r htmlextra --reporter-htmlextra-export .\
```


## Pruebas desde interfaz web
```bash
http://localhost:8080/swagger-ui/index.html
```
Tambien puedes abrir tu navegador y realizar algunas pruebas desde la interfaz interactiva
que levanta el proyecto, el swagger se encuentra enriquecido con algunos datos de ejemplo.

## Diagrama
El diagrama de clases del código se encuentra en:
```bash
.\diagram\ClassDiagram.png
```

## Swagger
Los controladores y DTO y algunas configuraciones fueron generadas con el el generador de codigo
openapi-generator-cli.jar, los archivos de configuracion para regenerar el codigo estan en 

```bash
.\swagger\configDemoGlobal.json
.\swagger\globallogic.yaml
.\swagger\openapi-generator-cli.jar
```

Si vuelves a generar el codigo puede que algunas validaciones se pierdan, ya que fueron escritas post-generacion

Si quieres regenerar te dejo un comando de template, las palabras en mayusculas debes cambiarlas segun corresponda
el generador se encuentra en se encuentra en la misma ruta.
```bash
java -jar RUTA_DEL_GENERADOR generate -i RUTA_SWAGGER -g spring -c RUTA_JSON_CON_CONFIGURACIONES_DEL_PROYECTO -o NOMBRE_CARPETA_RAIZ_DONDE_SE_GENERARÁ
```

## Observaciones
El mensaje de error al indicar que un usuario ya existe considero que no debiese dar la razon de porque no se pudo crear.
A nivel de respuesta el mensaje "Error creating user" y a nivel de log debiese revisarse la razon final. Sin embargo, concatene 
los mensajes de las exceptions ya que el enunciado pedia que dijiera que era porque ya estaba registrado.


```bash
cl.globallogic.api.domain.exception.BusinessException: Error creating user,User alredy exist on db
at cl.globallogic.api.domain.usescase.users.UserUseCase.createUser(UserUseCase.java:46) ~[main/:na]
at cl.globallogic.api.domain.controller.SignUpApiController.sign(SignUpApiController.java:41) ~[main/:na]
Caused by: cl.globallogic.api.infrastructure.exception.RepositoryException: User alredy exist on db
at cl.globallogic.api.infrastructure.adapter.UserServiceAdapter.createdUser(UserServiceAdapter.java:31) ~[main/:na]
at cl.globallogic.api.domain.usescase.users.UserUseCase.createUser(UserUseCase.java:44) ~[main/:na]
... 51 common frames omitted
```

## Calidad de codigo
El codigo fue revisado con sonarlint para la corrección de codesmells.

## Reportes y tests
Se ha utilizado jacoco para la generacion de reportes de unit test y codecoverage,
deje los reportes en las siguiente ruta para que no tengan que generarlos ustedes

```bash
.\reports\coveragereport.html
.\reports\testreport.html
```

De igual forma estos reportes son generados una vez ejecutados los unit tests y pueden ser ubicados en:

```bash
.\build\reports\jacoco\
.\build\reports\tests\
```

## Utilitarios
https://www.liquid-technologies.com/online-json-to-schema-converter

